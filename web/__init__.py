# -*- coding: utf-8 -*-
import logging
from web.app import create_app

from web.commission.models import Commission
from web.user.models import User

try:
    from web.settings_local import Config
except Exception as e:
    logging.exception("Exception: %(body)s", {'body': e})
    from troika.settings import Config

app = create_app(Config)
