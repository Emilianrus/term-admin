'use strict';

angular.module('term_admin').controller('CommissionController',
    function($scope, $http, $compile) {

  $scope.search = {'page':1};
  $scope.pagination = [];
  $scope.firm_list = [];
  $scope.firm_selected = "";

  $scope.searchCards = function(search) {
    var troika_id_param = '';
    if (!angular.isUndefined(search) && !angular.isUndefined(search.troika_id) && search.troika_id.length)
      troika_id_param = '?troika_id=' + search.troika_id;
    angular.element(location).attr('href','/card/' + troika_id_param);
  };

//Запрос на отображение табличных данных
$scope.getGridContent = function(search) {
  var url;
  if (angular.isUndefined(search.page)) search.page = 1;
  if (!angular.isUndefined(search.action)){
    url = window.location.pathname + '/' + search.action + '/';
  } else {
    url = window.location.pathname;
  }

  search.csrf_token = $scope.token;

  $http.post(url, search).success(function(data) {

    $scope.result = data.result;
    $scope.set_pagination(data);
  });
};

  //Тригер на запрос табличных данных по параметрам
  $scope.$watch('search.period + search.page + search.firm_id', function() {
    if (!$scope.search) return false;
    var search = $scope.search;
    $scope.setEmptyResult();
    $scope.getGridContent(search);
  });

  //Обнуление результата
  $scope.setEmptyResult = function() {
    $scope.result = {};
    $scope.pagination = [];
  };

  $scope.set_pagination = function(data) {
    $scope.pagination = [];
    var page_count = Math.ceil(data.count / data.items_per_page);
    
    var page_prev = {'page':data.page - 1, 'caption': '«', 'class':''};
    if (data.page == 1) {
      page_prev.page = data.page;
      page_prev.class = 'disabled';
    }
    $scope.pagination.push(page_prev);

    var max_page = page_count;
    var min_page = 1;
    if (page_count > 10) {
      max_page = data.page + 3;
      min_page = data.page - 3;

      if (min_page < 4) {
        min_page = 1;
        max_page = 8;
      } else {
        $scope.pagination.push({'page':1, 'caption': 1, 'class':''});
        $scope.pagination.push({'page': (min_page - 1), 'caption': '...', 'class':''});
      }

      if (page_count - max_page < 4 && page_count > 10) {
        max_page = page_count;
        min_page = page_count - 8;
      }
    }

    for (var i = min_page; i <= max_page; i++) {
      var adding_page = {'page':i, 'caption': i, 'class':''};
      if (data.page == i){
        adding_page.class = 'active';
      }
      $scope.pagination.push(adding_page);
    }

    if (page_count > 10 && max_page < page_count){
      $scope.pagination.push({'page': (max_page + 1), 'caption': '...', 'class':''});
      $scope.pagination.push({'page':page_count, 'caption': page_count, 'class':''});
    }

    var page_next = {'page':data.page + 1, 'caption': '»', 'class':''};
    if (data.page == page_count){
      page_next.page = data.page;
      page_next.class = 'disabled';
    }
    $scope.pagination.push(page_next);
  }

  //request list of firms for dropbox
  $scope.initFirmList = function() {

    $http.post('/commission/firm_list').success(function(data) {
      $scope.firm_list = data.firm_list;
      $scope.selectFirm($scope.firm_list[0]);
    });

    $scope.selectFirm = function(firm) {
      $scope.firm_selected = firm;
      $scope.search.firm_id = firm.id;
      $scope.search.page = 1;
    }
  }


});