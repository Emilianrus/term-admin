'use strict';

angular.module('term_admin', []);

angular.module('term_admin').config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('{[{');
  $interpolateProvider.endSymbol('}]}');
});
