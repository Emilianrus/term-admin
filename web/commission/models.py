# -*- coding: utf-8 -*-
import json

from flask.ext.login import current_user
from sqlalchemy.sql import func

#from web.extensions import cache
from web.database import Model, SurrogatePK, db
from web.helpers import date_helper


class Commission(SurrogatePK, Model):

    __tablename__ = 'commission'

    PER_PAGE = 30

    report_id = db.Column(db.Integer)
    firm_id = db.Column(db.Integer)
    creation_date = db.Column(db.DateTime, nullable=False)
    amount = db.Column(db.Integer)


    @staticmethod
    #@cache.cached(timeout=5)
    def consolidated_list(period='day', page=1, **kwargs):
        sort_order = 'creation_date desc'

        query = Commission.query

        if 'firm_id' in kwargs:
            query = query.filter(Commission.firm_id == kwargs['firm_id'])

        if period == 'month':
            query = query.group_by(
            'YEAR(creation_date), MONTH(creation_date)')
        elif period == 'week':
            query = query.group_by(
            'YEAR(creation_date), MONTH(creation_date), WEEK(creation_date)')
        else:
            query = query.group_by(
            'YEAR(creation_date), MONTH(creation_date), DAY(creation_date)')

        query = query.add_columns(func.count(Commission.id).label("count"))
        query = query.add_columns(func.sum(Commission.amount).label("summ"))

        grouped = query.order_by(sort_order).paginate(page, Commission.PER_PAGE, False)

        result = []

        for row in grouped.items:
            data = dict(
                summ = "%02d.%02d" % (row.summ / 100, row.summ % 100),
                count = row.count
            )

            if period == 'month':
                data['date'] = row.Commission.creation_date.strftime('%m.%Y')
            elif period == 'week':
                data['date'] = "%s - %s" % date_helper.week_borders(row.Commission.creation_date)
            else:
                data['date'] = row.Commission.creation_date.strftime('%d.%m.%Y')

            result.append(data)

        value = dict(
            result=result,
            page=page,
            count=query.count(),
            items_per_page=Commission.PER_PAGE
        )

        return value


class Firm(SurrogatePK, Model):

    __tablename__ = 'firm'
    
    COMMISSION_NULL = 0
    COMMISSION_BY_PERSON = 1
    COMMISSION_BY_TERM = 2

    name = db.Column(db.String(300), nullable=False)
    inn = db.Column(db.String(50))
    sub_domain = db.Column(db.Text(), nullable=False, index=True)
    pattern_id = db.Column(db.String(200), nullable=False)
    logo = db.Column(db.Text())
    address = db.Column(db.Text())
    account_email = db.Column(db.Text())
    transaction_percent = db.Column(db.Integer()) # до сотых долей процента, 1% = 100
    transaction_comission = db.Column(db.Integer())
    legal_entity = db.Column(db.String(256))
    general_manager = db.Column(db.String(128))
    chief_accountant = db.Column(db.String(128))
    gprs_rate = db.Column(db.Integer())
    contract = db.Column(db.String(256))
    commission_type = db.Column(db.Integer())


    @staticmethod
    def commission_list():

        firms = Firm.query.filter(Firm.commission_type > 0).all()

        answer = []
        for firm in firms:
            data = dict(
                id = firm.id,
                name = firm.name
                )
            answer.append(data)

        return answer


