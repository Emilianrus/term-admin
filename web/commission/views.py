# -*- coding: utf-8 -*-
import logging
import json

from flask import (Blueprint, abort, render_template,
                   request, redirect, jsonify, g)
from flask.ext.login import current_user, login_required

from web.commission.models import Commission, Firm

logger = logging.getLogger(__name__)

blueprint = Blueprint("commission", __name__, url_prefix='/commission',
                      static_folder="../static")



@blueprint.route("/consolidated", methods=['GET'])
@login_required
def consolidated():
    return render_template("commission/consolidated.html")


@blueprint.route("/consolidated", methods=['POST'])
@login_required
def consolidated_list():
    arg = json.loads(request.stream.read())

    period='day'
    if 'period' in arg:
        period = arg['period']

    
    page = 1
    if 'page' in arg:
        page = arg['page']

    answer = Commission.consolidated_list(period, page)

    return jsonify(answer)


@blueprint.route("/history", methods=['GET'])
@login_required
def history():
    return render_template("commission/history.html")


@blueprint.route("/history", methods=['POST'])
@login_required
def history_data():
    arg = json.loads(request.stream.read())

    if not 'firm_id' in arg:
        abort(403);

    firm_id = arg['firm_id']

    period='month'
    
    page = 1
    if 'page' in arg:
        page = arg['page']

    answer = Commission.consolidated_list(period, page, firm_id = firm_id)

    return jsonify(answer)


@blueprint.route("/firm_list", methods=['POST'])
@login_required
def firm_list():

    answer = {}
    answer['firm_list'] = Firm.commission_list()
    
    return jsonify(answer)

@blueprint.before_request
def before_request():
    g.user = current_user