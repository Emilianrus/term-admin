# -*- coding: utf-8 -*-
import datetime as dt
import hashlib
import time

from flask import current_app
from flask.ext.login import UserMixin

from web.database import (Column, Model, SurrogatePK, db)
from web.extensions import bcrypt


class User(UserMixin, SurrogatePK, Model):

    __tablename__ = 'term_user'

    GROUP_DEFAULT = 0
    GROUP_API = 1

    STATUS_NOACTIVE = 0
    STATUS_ACTIVE = 1
    STATUS_BANNED = -1

    email = db.Column(db.String(128), nullable=False, unique=True)
    password = db.Column(db.String(128), nullable=False)
    activkey = db.Column(db.String(128), nullable=False)
    creation_date = db.Column(db.DateTime, nullable=False)
    group = db.Column(db.Integer, nullable=False)
    lastvisit = db.Column(db.DateTime)
    api_key = db.Column(db.String(150))
    api_secret = db.Column(db.String(150))
    status = db.Column(db.Integer, index=True)

    def __init__(self, email=None, password=None, **kwargs):
        self.group = self.GROUP_DEFAULT
        self.status = self.STATUS_NOACTIVE
        self.creation_date = date_helper.get_current_date()
        if password:
            self.set_password(password)
            self.set_activkey(password)
        else:
            self.password = None

        db.Model.__init__(self, email=email, **kwargs)

    def check_password(self, value):
        return bcrypt.check_password_hash(self.password, value)

    def update_lastvisit(self):
        self.lastvisit = dt.datetime.utcnow()
        self.save()

    def __repr__(self):
        return '<User({email!r})>'.format(email=self.email)
