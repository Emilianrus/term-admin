# -*- coding: utf-8 -*-
from flask.ext.bcrypt import Bcrypt
from flask.ext.cache import Cache
from flask.ext.debugtoolbar import DebugToolbarExtension
from flask.ext.login import LoginManager
from flask.ext.sqlalchemy import SQLAlchemy

bcrypt = Bcrypt()

login_manager = LoginManager()

db = SQLAlchemy()

cache = Cache()

debug_toolbar = DebugToolbarExtension()
