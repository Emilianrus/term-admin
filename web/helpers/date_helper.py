# -*- coding: utf-8 -*-
from datetime import timedelta

   
def week_borders(date):
    day_of_week = date.weekday()

    to_beginning_of_week = timedelta(days=day_of_week)
    beginning_of_week = date - to_beginning_of_week

    to_end_of_week = timedelta(days=6 - day_of_week)
    end_of_week = date + to_end_of_week

    return (beginning_of_week.strftime('%d.%m.%Y'), end_of_week.strftime('%d.%m.%Y'))