# -*- coding: utf-8 -*-
from flask.ext.assets import Bundle, Environment

css = Bundle(
    "libs/bootstrap/dist/css/bootstrap.min.css",
    "css/bootstrap-cerulean.min.css",
    "css/style.css",
    filters="cssmin",
    output="public/css/common.css"
)

js = Bundle(
    "libs/jQuery/dist/jquery.min.js",
    "libs/angular/dist/angular.min.js",
    "libs/bootstrap/dist/js/bootstrap.min.js",
    "angular/app.js",
    "angular/controllers/commission.js",
    filters='jsmin',
    output="public/js/common.js"
)

assets = Environment()

assets.register("js_all", js)
assets.register("css_all", css)
